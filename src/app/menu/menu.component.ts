import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DISHES } from '../shared/dishes';
import { DishService } from '../services/dish.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class MenuComponent implements OnInit {

  dishes: Dish[];
  selectedDish: Dish;
  errMess: string;
  expand = 'shown';

  constructor(
    private dishService: DishService,
    @Inject('baseURL') private baseURL
  ) { }

  ngOnInit() {
    console.log(DISHES);

    this.dishService.getDishes()
      .subscribe(
        dishes => this.dishes = dishes,
        errmess => this.errMess = <any>errmess
      );
  }

  onSelect(dish: Dish) {
    console.log(dish)
    this.selectedDish = dish;
  }

}
