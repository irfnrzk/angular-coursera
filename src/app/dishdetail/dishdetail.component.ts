import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Feedback } from '../shared/feedback';
import { visibility, expand } from '../animations/app.animation';
import { flyInOut } from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ]
})

export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishIds: string[];
  prev: string;
  next: string;
  dishErrMess: string;
  dishcopy: Dish;
  visibility = 'shown';
  expand = 'shown';

  feedbackForm: FormGroup;
  feedback: Feedback;
  @ViewChild('fform') feedbackFormDirective;

  formErrors = {
    'author': '',
    'comment': ''
  };

  validationMessages = {
    'author': {
      'required': 'Author Name must be at least 2 characters long.',
      'minlength': 'First Name must be at least 2 characters long.',
      'maxlength': 'author cannot be more than 25 characters long.'
    },
    'comment': {
      'required': 'Comment is required.'
    },
  };

  sliderCfg = {
    autoTicks: false,
    max: 5,
    min: 0,
    showTicks: true,
    step: 1,
    thumbLabel: true
    // value: 0,
  }

  get tickInterval(): number | 'auto' {
    return this.sliderCfg.showTicks ? (this.sliderCfg.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
  private _tickInterval = 1;

  constructor(
    private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('baseURL') private baseURL
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.dishservice.getDishIds().subscribe(
      dishIds => this.dishIds = dishIds,
      dishErrMess => this.dishErrMess = <any>dishErrMess
    );

    this.route.params
      .pipe(switchMap((params: Params) => {
        this.visibility = 'hidden';
        return this.dishservice.getDish(+params['id']);
      }))
      .subscribe(dish => {
        this.dish = dish;
        this.dishcopy = dish;
        this.setPrevNext(dish.id);
        this.visibility = 'shown';
      },
        dishErrMess => this.dishErrMess = <any>dishErrMess);
  }

  createForm(): void {
    this.feedbackForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: 5,
      comment: ['', [Validators.required]],
      date: new Date()
    });

    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }
  onSubmit() {
    this.feedback = this.feedbackForm.value;
    this.dish.comments.push(this.feedbackForm.value);
    this.dishservice.putDish(this.dishcopy)
      .subscribe(dish => {
        this.dish = dish; this.dishcopy = dish;
      },
        dishErrMess => {
          this.dish = null;
          this.dishcopy = null;
          this.dishErrMess = <any>dishErrMess;
        });

    this.feedbackFormDirective.resetForm();
    this.feedbackForm.reset({
      author: '',
      rating: 5,
      comment: '',
      date: new Date
    });
  }

}